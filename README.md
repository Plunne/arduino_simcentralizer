# Sim Centralizer

Automotive Simulator peripherals USB Hub in Arduino for Leonardo/Micro pro boards.

# Setup

1. Clone this repo.

2. Copy the librairies into your arduino libraries directory.

3. Open the .ino with Arduino IDE.

4. Upload it to your board.

5. Enjoy!


# Patch Notes

### Version 0.1

- Only 6 speed available for reading shifter.
- Display working and ready for 7 gears + Reverse.
- Neutral working
- Handbrake available

# Credit

Made by Lena SAVY-LARIGALDIE <br/>
HID lib used by NicoHood : https://github.com/NicoHood/HID
