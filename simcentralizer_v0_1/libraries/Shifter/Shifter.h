#ifndef SHIFTER_H
#define SHIFTER_H

#include <Arduino.h>

class Shifter
{
    /* Initialisation */

    // Pins
    int pinAxisX = 0;
    int pinAxisY = 0;

    // Axis
    int _Xaxis = 0;
    int _Yaxis = 0;

    // Axis Limits
    const int _AXISLOW = 480;
    const int _AXISHIGH = 530;

    // Gear
    int _gear = 0;

public:
    Shifter();
    Shifter(int pinX, int pinY);
    // Unit Test
    void setTestShifter(int x, int y);
    void getTestShifter();
    // H Pattern
    void shifterHgear1();
    void shifterHgear2();
    void shifterHgear3();
    void shifterHgear4();
    void shifterHgear5();
    void shifterHgear6();
    void shifterHgearNeutral();
    // H Zone
    void shifterHgear12();
    void shifterHgear34();
    void shifterHgear56();
    // Select gear
    int getGear();

};

#endif // JOYSTICK_H
