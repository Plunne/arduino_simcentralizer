#include "Shifter.h"

/****************
 * CONSTRUCTORS *
 ****************/

Shifter::Shifter()
{
}

Shifter::Shifter(int pinX, int pinY)
{
    // Init Axis Pins with constructor values
    pinAxisX = pinX;
    pinAxisY = pinY;

    // Init Pin Modes for the display
    pinMode(pinAxisX, INPUT);
    pinMode(pinAxisY, INPUT);
}


/*************
 * H Pattern *
 *************/

void Shifter::shifterHgear1(){
    // Y+
    if(_Yaxis > _AXISHIGH)
        _gear = 1;
}

void Shifter::shifterHgear2(){
    // Y-
    if(_Yaxis < _AXISLOW)
        _gear = 2;
}

void Shifter::shifterHgear3(){
    // Y+
    if(_Yaxis > _AXISHIGH)
        _gear = 3;
}

void Shifter::shifterHgear4(){
    // Y-
    if(_Yaxis < _AXISLOW)
        _gear = 4;
}

void Shifter::shifterHgear5(){
    // Y+
    if(_Yaxis > _AXISHIGH)
        _gear = 5;
}

void Shifter::shifterHgear6(){
    // Y-
    if(_Yaxis < _AXISLOW)
        _gear = 6;
}

void Shifter::shifterHgearNeutral(){
    // Y-
    if((_Yaxis > _AXISLOW) && (_Yaxis < _AXISHIGH))
        _gear = 0;
}

/**********
 * H Zone *
 **********/
void Shifter::shifterHgear12(){
    // X-
    if(_Xaxis < _AXISLOW){
        shifterHgear1();
        shifterHgear2();
        shifterHgearNeutral();
    }
}

void Shifter::shifterHgear34(){
    // X=
    if((_Xaxis > _AXISLOW) && (_Xaxis < _AXISHIGH)){
        shifterHgear3();
        shifterHgear4();
        shifterHgearNeutral();
    }
}

void Shifter::shifterHgear56(){
    // X+
    if((_Xaxis > _AXISHIGH)){
        shifterHgear5();
        shifterHgear6();
        shifterHgearNeutral();
    }
}

/************
 * Get Gear *
 ************/
int Shifter::getGear(){

    // Read the axis values
    _Xaxis = analogRead(pinAxisX);
    _Yaxis = analogRead(pinAxisY);

    // Define the gear
    shifterHgear12();
    shifterHgear34();
    shifterHgear56();
    
    return _gear;
}
