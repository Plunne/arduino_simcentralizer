#ifndef DISPLAY_H
#define DISPLAY_H

#include <Arduino.h>
#include <string.h>

class Display
{
    int ANODEA = 0;
    int ANODEB = 0;
    int ANODEC = 0;
    int ANODED = 0;
    int ANODEE = 0;
    int ANODEF = 0;
    int ANODEG = 0;

public:
    Display();
    Display(int a, int b, int c, int d, int e, int f, int g);
    
    void displayGear(int n);
};

#endif // DISPLAY_H
