#include "Display.h"

/*******************
 * GLOBAL VAIABLES *
 *******************/

char _tabGear[] = {0,0,0,0,0,0,0};

/****************
 * CONSTRUCTORS *
 ****************/

Display::Display()
{
}

Display::Display(int a, int b, int c, int d, int e, int f, int g)
{
    // Init Anode Pins with constructor values
    ANODEA = a;
    ANODEB = b;
    ANODEC = c;
    ANODED = d;
    ANODEE = e;
    ANODEF = f;
    ANODEG = g;

    // Init Pin Modes for the display 
    pinMode(ANODEA, OUTPUT);
    pinMode(ANODEB, OUTPUT);
    pinMode(ANODEC, OUTPUT);
    pinMode(ANODED, OUTPUT);
    pinMode(ANODEE, OUTPUT);
    pinMode(ANODEF, OUTPUT);
    pinMode(ANODEG, OUTPUT);
}

/*************
 * FUNCTIONS *
 *************/

void Display::displayGear(int n){

    // 7 segments codes
    switch(n)
    {
        case 0:
            strcpy(_tabGear,"0010101"); // n
            break;
        case 1:
            strcpy(_tabGear,"0110000"); // 1
            break;
        case 2:
            strcpy(_tabGear,"1101101"); // 2
            break;
        case 3:
            strcpy(_tabGear,"1111001"); // 3
            break;
        case 4:
            strcpy(_tabGear,"0110011"); // 4
            break;
        case 5:
            strcpy(_tabGear,"1011011"); // 5
            break;
        case 6:
            strcpy(_tabGear,"1011111"); // 6
            break;
        case 7:
            strcpy(_tabGear,"1110010"); // 7
            break;
        case 8:
            strcpy(_tabGear,"0000101"); // r
            break;
        default:
            strcpy(_tabGear,"1111110"); // 0
            break;
    }

    // Char -> Int
    for(int i=0; i<7; i++)
        _tabGear[i] -= '0';

    // Write tab values in Display
    digitalWrite(ANODEA, _tabGear[0]);
    digitalWrite(ANODEB, _tabGear[1]);    
    digitalWrite(ANODEC, _tabGear[2]);    
    digitalWrite(ANODED, _tabGear[3]);  
    digitalWrite(ANODEE, _tabGear[4]);    
    digitalWrite(ANODEF, _tabGear[5]);    
    digitalWrite(ANODEG, _tabGear[6]);

}
