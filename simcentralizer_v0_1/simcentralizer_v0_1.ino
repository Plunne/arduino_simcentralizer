#include "HID-Project.h"

#include <Shifter.h>
#include <Display.h>

#define SHIFTER_Y A0
#define SHIFTER_X A1
#define HANDBRAKE A2

Display Display(2,3,4,5,6,7,8); // Numbers are the pins
Shifter Shifter(SHIFTER_X, SHIFTER_Y); // X = A1 ; Y = A0
unsigned char gear = 0;

void setup() {

  // Sends a clean report to the host. This is important on any Arduino type.
  pinMode(HANDBRAKE, INPUT);
  Gamepad.begin();
}

void loop() {

    // Set the gear variable of Gear object
    // from Shifter Joystick gear number returned by getGear()
    gear = Shifter.getGear();

    Gamepad.releaseAll();
    
    if(gear != 0)
        Gamepad.press(gear);

    // Create the Handbrake
    Gamepad.rxAxis(analogRead(HANDBRAKE));

    // Functions above only set the values.
    // This writes the report to the host.
    Gamepad.write();

    // Display the gear obteined from last step
    Display.displayGear(gear);
}
